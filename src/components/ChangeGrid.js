import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class ChangeGrid extends Component {
    state = {
      options: [
        {
          name: '4x',
          value: 4,
        },
        {
          name: '5x',
          value: 5,
        },
        {
          name: '6x',
          value: 6,
        }
      ],
      value: 'GridMatrix',
      dropdownOpen:false
    };
  
    handleChange = (event) => {
        console.log(event.target);
        this.setState({ value: event.target.innerText });
      this.props.onSelectGrid(event.target.value); 
    };

    toggle = (event) => {
        this.setState({ dropdownOpen: !this.state.dropdownOpen });
      
    };
   
    render() {
      const { options, value } = this.state;
  
      return (

        <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
      <DropdownToggle caret>
        {this.state.value}
        </DropdownToggle>
        <DropdownMenu>
        {options.map(item => (
                     <DropdownItem key={item.value} value={item.value} onClick={this.handleChange}>  {item.name}</DropdownItem>
             
            ))}
        </DropdownMenu>
      </Dropdown>

       
      );
    }
  }
  export {ChangeGrid}