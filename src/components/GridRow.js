

  import React from 'react';
  import { GridCell } from './GridCell';
  const GridRow = ({ row }) => {
    return (
      <div className="row">
        {row.map((cell, i) => (<GridCell key={i} cellValue={cell} />))}
      </div>
    );
  };
  

  export {GridRow}