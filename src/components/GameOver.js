import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class GameOver extends Component {
    state = {

        value: ' Game over. Please start a new game.',
    };
    render() {

        return (

            <div className="game_over">
                {this.state.value}
            </div>


        );
    }
}
export { GameOver }