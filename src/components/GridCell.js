import React from 'react';
const GridCell = ({ cellValue }) => {
    let color = 'cell';
    let value = (cellValue === 0) ? '' : cellValue;
    if (value) {
      color += ` color-${value}`;
    }
  
    return (
    
        <div className={color}>
          <div className="number">{value}</div>
        </div>
     
    );
  };

  export {GridCell}